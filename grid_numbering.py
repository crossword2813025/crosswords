LB = "\n"
WHITE = '_'
SIZE = 15 


def is_across(grid: list[str], row: int, col: int) -> bool:
    if col == SIZE - 1:
        return False
    elif col == 0:
        return grid[row][col + 1] == WHITE
    return grid[row][col - 1] != "_" and grid[row][col + 1] == WHITE


def is_down(grid : list[str], row : int, col : int) -> bool :
    if row == SIZE - 1 :
        return False
    elif row == 0 :
        return grid[row + 1][col] == WHITE
    return grid[row - 1][col] != WHITE and grid[row + 1][col] == WHITE


def is_white(row: int, col: int) -> bool:
    return grid[row][col] == '_'
    

def numbering(grid: list[str]) -> str:
    clue = 1
    output = ''
    for row in range(SIZE):
        for col in range(SIZE):
            if is_white(row, col) and (is_across(grid, row, col) or is_down(grid, row, col)):
                output += f'{(row, col, clue)}\n'
                clue += 1
    return output


grid = ['________#______'\
       ,'_#_#_#_###_#_#_'\
       ,'________#______'\
       ,'_#_#_#_#_#_#_#_'\
       ,'___________####'\
       ,'_#_###_#_#_#_#_'\
       ,'_____#_________'\
       ,'_#_#_#_#_#_#_#_'\
       ,'_________#_____'\
       ,'_#_#_#_#_###_#_'\
       ,'####___________'\
       ,'_#_#_#_#_#_#_#_'\
       ,'______#________'\
       ,'_#_#_###_#_#_#_'\
       ,'______#________']


print(numbering(grid))

















